#include "cliente.hpp"

bool file_exists_cliente(const string &name, string CPF){
    CPF += ".txt";
    vector<string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);

    for(string NomeArq : v){
        if(NomeArq == CPF) return true;
    }

    return false;
}

Cliente::Cliente(){
    set_NomeCompleto("");
    set_CPF("");
    this->socio = 0;
}

Cliente::~Cliente(){

}

void Cliente::set_socio(bool socio){
    this->socio = socio;
}

bool Cliente::get_socio(){
    return socio;
}

bool Cliente::cadastro(){
    ofstream saida;
    string cpf, nome_completo;
    string y_or_n;
    system("clear");
    cout << "CADASTRAR CLIENTE" << endl << endl;
    
    cout << "Insira o CPF:" << endl;
    cin >> cpf;
    set_CPF(cpf);

    if(file_exists_cliente("../ep1/doc/Clientes/", get_CPF())){
        system("clear");
        cout << "Cliente já cadastrado!" << endl << endl;
        return true;
    }else{
        saida.open("../ep1/doc/Clientes/" + get_CPF() + ".txt");
        if(saida.is_open()){
            cout << "Insira o nome completo do cliente:" << endl;
            getline(cin >> ws, nome_completo);
            set_NomeCompleto(nome_completo);
            cout << "O(A) Cliente é sócio(a)? [y/n]" << endl;
            cin >> y_or_n;
            if(y_or_n == "y") socio = 1;
            saida << nome_completo << endl << socio;
            saida.close();
            system("clear");
            cout << "Cliente cadastrado com sucesso!" << endl << endl;
            return true;
        }
    }
    return false;
}