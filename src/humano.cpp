#include "humano.hpp"


Humano::Humano(){
    this->NomeCompleto = "";
    this->CPF = "";
}

Humano::~Humano(){

}

void Humano::set_NomeCompleto(string NomeCompleto){
    this->NomeCompleto = NomeCompleto;
}

void Humano::set_CPF(string CPF){
    this->CPF = CPF;
}

string Humano::get_NomeCompleto(){
    return NomeCompleto;
}

string Humano::get_CPF(){
    return CPF;
}

bool Humano::cadastro(){
    return true;
}