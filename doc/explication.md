Para acessar o sistema da loja, é preciso que o usuário seja um funcionário da loja. Isto é, no primeiro acesso ao sistema, faz-se necessário o cadastro do funcionário.

Após feito o cadastro do funcionário, o mesmo poderá acessar o menu do sistema e realizar as ações exibidas na tela de comando.

O modo Estoque está embutido no Cadastro de Produtos e o modo Venda e o modo Recomendação estão visíveis nas opções do menu.

De forma geral, o uso do sistema é bastante intuitivo e simples. Deve se atentar apenas a algumas observações:
-> Para perguntas de "sim" ou "não", digite "y" como resposta para "sim" e "n" como resposta para "não";
-> Para a escolha de uma alternativa em "Escolha uma opção:", digite apenas o número que corresponde a opção desejada. Ex: 1, 2, 3.