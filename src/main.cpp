#include "funcionario.hpp"
#include "carrinho.hpp"
#include "cliente.hpp"


void menu();
bool file_exists_menu(const string &name, string CPF);
bool existe_cliente();


int main(){
    
    Funcionario funcionarios;

    bool login_funcionario = funcionarios.cadastro();

    if(login_funcionario){
        
        int opcao;
        system("clear");
        menu();
        cout << endl;
        cin >> opcao;

        do{
            switch(opcao){

                case 1:{
                    Cliente clientes;
                    bool cadastro_cliente = clientes.cadastro();
                    if(cadastro_cliente){
                        menu();
                        cout << endl;
                        cin >> opcao;
                    }
                    break;
                }

                case 2:{
                    Produto produtos;
                    bool cadastro_produto = produtos.cadastro();
                    if(cadastro_produto){
                        menu();
                        cout << endl;
                        cin >> opcao;
                    }
                    break;
                }

                case 3:{
                    Carrinho vendas;
                    bool voltar_para_menu = vendas.venda();
                    bool venda_realizada = vendas.true_or_false();
                    if(venda_realizada){
                        return 0;
                    }else if(voltar_para_menu){
                        menu();
                        cout << endl;
                        cin >> opcao;
                    }
                    break;
                }

                case 4:{
                    if(existe_cliente()){
                        system("clear");
                        menu();
                        cout << endl;
                        cin >> opcao;
                    }
                    break;
                }

                case 5:{
                    login_funcionario = funcionarios.cadastro();
                    if(login_funcionario){
                        system("clear");
                        menu();
                        cout << endl;
                        cin >> opcao;
                    }
                    break;
                }

                default:
                    break;

            }
        }while(opcao != 6);
    }

    cout << endl;
    cout << "Bye bye! Execute o programa novamente caso queira realizar uma nova venda." << endl;

    return 0;
}


void menu(){

    cout << "+-+-+-+-+-+ MENU +-+-+-+-+-+" << endl;
    cout << "Selecione uma opção:" << endl << endl;
    cout << "1 - Cadastrar cliente" << endl;
    cout << "2 - Cadastrar produto" << endl;
    cout << "3 - Modo venda" << endl;
    cout << "4 - Modo recomendação" << endl;
    cout << "5 - Trocar de funcionário" << endl;
    cout << "6 - Sair" << endl;

}

bool file_exists_menu(const string &name, string CPF){
    CPF += ".txt";
    vector<string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);

    for(string NomeArq : v){
        if(NomeArq == CPF) return true;
    }

    return false;
}

bool existe_cliente(){

    string cpf, texto1, texto2;
    int temp;
    ofstream saida;
    ifstream entrada1;
    ifstream entrada2;

    cout << "Para entrar no modo recomendação, informe o CPF do cliente." << endl << "Digite o CPF:" << endl;
    cin >> cpf;
    if(!file_exists_menu("../ep1/doc/Clientes/", cpf)){
        do{
            cout << "Cliente não cadastrado! Selecione uma opcao:" << endl;
            cout << "1 - Tentar novamente." << endl << "2 - Voltar para o menu." << endl;
            cin >> temp;
            if(temp == 1){
            cout << "Digite o CPF do cliente novamente:" << endl;
            cin >> cpf;
            }else{
                return true;
            }
        }while(!file_exists_menu("../ep1/doc/Clientes/", cpf));
    }else{
        system("clear");
        cout << "MODO RECOMENDAÇÃO" << endl << endl;
        cout << "Recomendações para o(a) cliente ";
        entrada1.open("../ep1/doc/Clientes/" + cpf + ".txt");
        if(entrada1.is_open()){
            if(getline(entrada1, texto1)){
                cout << texto1 << ":" << endl << endl;
            }

            cout << "+-+-+-+-+-+-+-+-+-+ RECOMENDAÇÕES +-+-+-+-+-+-+-+-+-+" << endl << endl;

            temp = 0;
            for(int line = 1; getline(entrada1,texto1); line++){

                if(line == 1){}
                if(line >= 2){
                    entrada2.open("../ep1/doc/Categorias/" + texto1 + ".txt");
                    if(entrada2.is_open()){
                        while(getline(entrada2, texto2)){
                            temp++;
                            if(temp == 10){
                                cout << endl << "Digite ENTER para voltar ao menu." << endl;
                                getchar(); getchar();
                                return true;
                            }
                            cout << texto2 << endl;
                        }
                        entrada2.close();
                    }
                }
            }       
            cout << endl << "Digite ENTER para voltar ao menu." << endl;
            getchar(); getchar();  
            return true;
            entrada1.close();
        }
    }
    return false;
}