#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <sys/types.h>
#include <dirent.h>
#include "produto.hpp"
#include "cliente.hpp"

class Carrinho{

    public:
        Carrinho();
        ~Carrinho();
        void add_produto();
        void remove_produto();
        bool venda();
        void imprime_carrinho();
        bool true_or_false();

};

#endif