#include "produto.hpp"

Produto::Produto(){
    this->nome = "";
    this->categoria = "";
    this->preco = 0.0;
    this->quantidade_estoque = 0;
    this->ID = "";
}

Produto::~Produto(){
    
}

void Produto::set_nome(string nome){
    this->nome = nome;
}

void Produto::set_categoria(string categoria){
    this->categoria = categoria;
}

void Produto::set_ID(string ID){
    this->ID = ID;
}

void Produto::set_preco(float preco){
    this->preco = preco;
}

void Produto::set_quantidade_estoque(int quantidade_estoque){
    this->quantidade_estoque = quantidade_estoque;
}

string Produto::get_nome(){
    return nome;
}

string Produto::get_categoria(){
    return categoria;
}

string Produto::get_ID(){
    return ID;
}

float Produto::get_preco(){
    return preco;
}

int Produto::get_quantidade_estoque(){
    return quantidade_estoque;
}

bool Produto::cadastro(){

    ifstream entrada;
    ofstream saida;
    string texto;
    int quantidade_categorias;

    system("clear");
    cout << "CADASTRAR PRODUTO" << endl << endl;
    cout << "Para cadastrar o produto, informe:" << endl;
    cout << "ID: "; cin >> ID;
    cout << "Nome: "; getline(cin >> ws, nome);
    cout << "Preço: "; cin >> preco;
    cout << "Quantidade em estoque: "; cin >> quantidade_estoque;
    
    saida.open("../ep1/doc/Produtos/" + ID + ".txt");
    if(saida.is_open()){
        saida << nome << endl << preco << endl << quantidade_estoque;
        saida.close();
    }

    cout << "O produto possui quantas categorias? "; cin >> quantidade_categorias;
    for(int i=1; i<=quantidade_categorias; i++){
        cout << "Categoria " << i << ": ";
        getline(cin >>  ws, categoria);

        saida.open("../ep1/doc/Produtos/" + ID + ".txt", ios_base::app);
        if(saida.is_open()){
            saida << endl << categoria;
            saida.close();
        }

        saida.open("../ep1/doc/Categorias/" + categoria + ".txt", ios_base::app);
        if(saida.is_open()){
            entrada.open("../ep1/doc/Categorias/" + categoria + ".txt");
            if(entrada.is_open()){
                while(entrada >> texto){
                    if(texto == ID){
                        saida.close();
                        entrada.close();
                        break;
                    }
                }
                saida << nome << endl;
                saida.close();
                entrada.close();
            }
        }
    }

    system("clear");
    cout << "Produto cadastrado com sucesso!" << endl << endl;
    return true;
}