#include "carrinho.hpp"

vector<string> produtos = {};
vector<int> quantidades = {};
int socio;
string texto, cpf;
vector<string> categorias = {};

bool t_or_f;
bool Carrinho::true_or_false(){
    return t_or_f;
}

bool file_exists_carrinho(const string &name, string CPF){
    CPF += ".txt";
    vector<string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);

    for(string NomeArq : v){
        if(NomeArq == CPF) return true;
    }

    return false;
}

bool text_exists_carrinho(string name, string dado){

    ifstream entrada;
    string texto;

    entrada.open("../ep1/doc/Clientes/" + name + ".txt");

    if(entrada.is_open()){
        while(getline(entrada, texto)){
            if(texto == dado){
                entrada.close();
                return true;
            }
        }
    }
    return false;
}

Carrinho::Carrinho(){
    
}

Carrinho::~Carrinho(){

}

void Carrinho::imprime_carrinho(){

    ifstream entrada;
    ofstream saida;
    string nome_produto = "", texto;
    float preco_produto = 0.0, valor_total = 0.0, valor_final = 0.0;
    int qtd_produto_estoque = 0, line;
    unsigned int i;

    for(i = 0; i < produtos.size(); i++){
        entrada.open("../ep1/doc/Produtos/" + produtos[i] + ".txt");
        if(entrada.is_open()){
            for(line = 1; getline(entrada, texto) && line <= 3; line++){
                if(line == 3) qtd_produto_estoque = stoi(texto);
            }
            if(quantidades[i] > qtd_produto_estoque){
                produtos.clear();
                quantidades.clear();
                system("clear");
                cout << "ERROR! A compra apresenta algum produto em quantidade maior que a existente no estoque. Compra cancelada!" << endl << endl;
                cout << "Execute o programa novamente caso queira realizar uma nova venda." << endl;
                t_or_f = true;
                return;
            } 
        }
        entrada.close();
    }
    qtd_produto_estoque = 0;

    system("clear");
    cout << "Venda realizada com sucesso! Logo abaixo mostrará o carrinho e o valor a ser pago." << endl << endl;

    cout << "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ CARRINHO +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" << endl << endl;

    for(i = 0; i < produtos.size(); i++){
        entrada.open("../ep1/doc/Produtos/" + produtos[i] + ".txt");
        if(entrada.is_open()){
            for(line = 1; getline(entrada, texto); line++){
                if(line == 1) nome_produto = texto;
                if(line == 2) preco_produto = stof(texto);
                if(line == 3) qtd_produto_estoque = stoi(texto);
                if(line >= 4) categorias.push_back(texto);
            }
            entrada.close();

            cout << produtos[i] << " - " << nome_produto << ": " << quantidades[i] << " x R$" << preco_produto << endl;
            valor_total = (quantidades[i]*preco_produto) + valor_total;
            
            saida.open("../ep1/doc/Produtos/" + produtos[i] + ".txt");
            if(saida.is_open()){
                saida << nome_produto << endl << preco_produto << endl << qtd_produto_estoque - quantidades[i];
                for(unsigned int j = 0; j < categorias.size(); j++){
                    saida << endl << categorias[j];
                }
                saida.close();
            }

            saida.open("../ep1/doc/Clientes/" + cpf + ".txt", ios_base::app);
            for(i = 0; i < categorias.size(); i++){
                if(saida.is_open()){
                    saida << endl << categorias[i];
                }
            }

        }
    }
    
    cout << "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ CARRINHO +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" << endl << endl;

    nome_produto = "";
    preco_produto = 0.0;
    qtd_produto_estoque = 0;

    cout << "O valor total é de R$ " << valor_total << endl;
    if(socio == 1){
        valor_final = valor_total - (valor_total * 0.15);
        cout << "Mas o(a) cliente é sócio(a)! Por tanto, há 15% de desconto no valor total da compra!" << endl;
        cout << "Sendo assim, o valor a ser pago é de R$ " << valor_final << endl << endl;
        valor_final = 0.0;
        valor_total = 0.0;
        t_or_f = true;
        return;
    }else{
        cout << "Como o(a) cliente não é sócio(a), não há desconto na compra." << endl;
        cout << "Sendo assim, o valor a ser pago é o mesmo do valor total: R$ " << valor_total << endl << endl;
        valor_total = 0.0;
        t_or_f = true;
        return;
    }
}

void Carrinho::add_produto(){

    ifstream entrada;
    string ID_produto_add, nome_produto;
    int qtd_produto_estoque = 0, qtd_produto_desejado;
    float preco_produto = 0;
    int op, cont = 0;

    system("clear");
    cout << "ADICIONANDO PRODUTOS AO CARRINHO" << endl << endl;

    if(produtos.empty()){
        cout << "O carrinho está vazio! Adicione um produto ao carrinho." << endl << endl;
        cout << "Digite o ID do produto que deseja adicionar: "; cin >> ID_produto_add;
    }else{
        cout << "Digite o ID do produto que deseja adicionar: "; cin >> ID_produto_add;
    }

    if(file_exists_carrinho("../ep1/doc/Produtos/", ID_produto_add)){

        entrada.open("../ep1/doc/Produtos/" + ID_produto_add + ".txt");
        if(entrada.is_open()){
            for(int line = 1; getline(entrada, texto) && line <= 3; line++){
                if(line == 1) nome_produto = texto;
                if(line == 2) preco_produto = stof(texto);
                if(line == 3) qtd_produto_estoque = stoi(texto);
            }
            entrada.close();
            system("clear");
            cout << "Produto encontrado! Segue abaixo as informações referentes ao produto de ID " << ID_produto_add << endl;
            cout << "Nome do produto: " << nome_produto << endl;
            cout << "Preço de cada und: R$ " << preco_produto << endl;
            cout << "Quantidade em estoque: " << qtd_produto_estoque << endl << endl;
            cout << "Quantas und de " << nome_produto << " deseja adicionar ao carrinho?" << endl;
            cin >> qtd_produto_desejado;
            
            if(!produtos.empty()){
                for(unsigned int i=0; i<produtos.size(); i++){
                    if(produtos[i] == ID_produto_add){
                        cont = 1;
                        quantidades[i] += qtd_produto_desejado;
                        break;
                    }
                }
                if(cont == 0){
                    produtos.push_back(ID_produto_add);
                    quantidades.push_back(qtd_produto_desejado);
                }else{
                    cont = 0;
                }
            }else{
                produtos.push_back(ID_produto_add);
                quantidades.push_back(qtd_produto_desejado);
            }

            if(qtd_produto_desejado > 1){
                cout << endl;
                cout << "Foram adicionados " << qtd_produto_desejado << " und de " << nome_produto << " ao carrinho!" << endl << endl;
            }else{
                cout << endl;
                cout << "Foi adicionado 1 und de " << nome_produto << " ao carrinho!" << endl << endl;
            }

            cout << "Escolha uma opção:" << endl;
            cout << "1 - Adicionar mais um produto ao carrinho." << endl;
            cout << "2 - Remover um produto do carrinho." << endl;
            cout << "3 - Finalizar a venda." << endl;
            cin >> op;

            if(op == 1){
                add_produto();
            }
            else if(op == 2){
                remove_produto();
            }
            else{
                imprime_carrinho();
            }
        }
    }else{
        cout << "Produto não existe no estoque!" << endl;
        cout << "Digite ENTER para adicionar outro produto ao carrinho." << endl;
        getchar(); getchar();
        add_produto();
    }
}

void Carrinho::remove_produto(){

    ifstream entrada;
    string ID_produto_remove, nome_produto;
    int op;

    system("clear");
    cout << "REMOVENDO PRODUTOS DO CARRINHO" << endl << endl;
    if(produtos.empty()){
        cout << "O carrinho já está vazio! Digite ENTER para ir ao modo de adição de produtos ao carrinho." << endl;
        getchar(); getchar();
        add_produto();
    }else{
        cout << "Digite o ID do produto que deseja remover do carrinho: "; cin >> ID_produto_remove;
        for(unsigned int i = 0; i < produtos.size(); i++){
            if(produtos[i] == ID_produto_remove){
                produtos.erase(produtos.begin()+i);
                quantidades.erase(quantidades.begin()+i);
                entrada.open("../ep1/doc/Produtos/" + ID_produto_remove + ".txt");
                if(entrada.is_open()){
                    if(getline(entrada, texto)){
                        nome_produto = texto;
                    }
                    cout << endl;
                    cout << "Produto " << nome_produto << " foi removido do carrinho." << endl << endl;
                    cout << "Escolha uma opção:" << endl;
                    cout << "1 - Adicionar um produto ao carrinho." << endl;
                    cout << "2 - Finalizar a venda." << endl;
                    cin >> op;
                    if(op == 1){
                        add_produto();
                    }else{
                        imprime_carrinho();
                    }
                    entrada.close();
                }
            }else{
                cout << endl;
                cout << "Este produto não existe no carrinho." << endl << endl;
                cout << "Escolha uma opção:" << endl;
                cout << "1 - Adicionar um produto ao carrinho." << endl;
                cout << "2 - Finalizar a venda." << endl;
                cin >> op;
                if(op == 1){
                    add_produto();
                }else{
                    imprime_carrinho();
                }
            }
        }
    }
}

bool Carrinho::venda(){

    ifstream entrada;
    string y_or_n;
    int temp;

    system("clear");

    int check;
    char directory_is_empty[100];
    FILE *output;

    output = popen("cd ~/Documents/OO_2019/ep1/doc; ls Produtos | wc -l", "r");
    fgets(directory_is_empty, 100, output);
    pclose(output);
    check = atoi(directory_is_empty);
    if(check == 0){
        system("clear");
        cout << "Não foi possível realizar uma venda, pois não há produtos em estoque!" << endl << endl;
        return true;
    }


    cout << "MODO VENDA" << endl << endl;
    cout << "Para realizar a venda, informe o CPF do cliente." << endl;
    cout << "Digite o CPF:" << endl;
    cin >> cpf;
    if(!file_exists_carrinho("../ep1/doc/Clientes/", cpf)){
        do{
            cout << endl;
            cout << "Cliente não cadastrado! Escolha uma opcao:" << endl;
            cout << "1 - Tentar novamente." << endl;
            cout << "2 - Voltar ao menu para cadastrar o cliente." << endl;
            cin >> temp;
            if(temp == 1){
                cout << endl;
                cout << "Digite o CPF do cliente novamente:" << endl;
                cin >> cpf;
            }else{
                system("clear");
                return true;
            }
        }while(!file_exists_carrinho("../ep1/doc/Clientes/", cpf));
    }else{
        cout << endl;
        cout << "Cliente ";
        entrada.open("../ep1/doc/Clientes/" + cpf + ".txt");
        if(entrada.is_open()){
            if(getline(entrada, texto)){
                cout << texto << " está cadastrado(a)";
            }
            if(text_exists_carrinho(cpf, "1")){
                socio = 1;
                cout << " e é sócio(a)!" << endl << endl;
            }else{
                socio = 0;
                cout << " mas não é sócio(a)." << endl << endl;
            }
            entrada.close();
            cout << "Digite ENTER para ir ao modo de adição de produtos ao carrinho." << endl;
            getchar(); getchar();
            add_produto();
        }
    }
    return false;
}