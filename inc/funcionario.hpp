#ifndef FUNCIONARIO_HPP
#define FUNCIONARIO_HPP

#include "humano.hpp"

class Funcionario : public Humano{

    private:
        string senha;
    
    public:
        Funcionario();
        ~Funcionario();
        void set_senha(string senha);
        string get_senha();
        bool cadastro();

};


#endif