#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <sys/types.h>
#include <dirent.h>
#include <bits/stdc++.h>

using namespace std;

class Produto{

    private:
        string nome;
        string categoria;
        string ID;
        float preco;
        int quantidade_estoque;

    public:
        Produto();
        ~Produto();
        void set_nome(string nome);
        void set_categoria(string categoria);
        void set_ID(string ID);
        void set_preco(float preco);
        void set_quantidade_estoque(int quantidade_estoque);
        string get_nome();
        string get_categoria();
        string get_ID();
        float get_preco();
        int get_quantidade_estoque();
        bool cadastro();

};

#endif