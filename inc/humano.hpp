#ifndef HUMANO_HPP
#define HUMANO_HPP

#include <sys/types.h>
#include <dirent.h>
#include <bits/stdc++.h>

using namespace std;

class Humano{

    private:
        string NomeCompleto;
        string CPF;
    
    public:
        Humano();
        ~Humano();
        void set_NomeCompleto(string NomeCompleto);
        void set_CPF(string CPF);
        string get_NomeCompleto();
        string get_CPF();
        virtual bool cadastro();

};

#endif