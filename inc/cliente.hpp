#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include "humano.hpp"


class Cliente : public Humano{

    private:
        bool socio;

    public:
        Cliente();
        ~Cliente();
        void set_socio(bool socio);
        bool get_socio();
        bool cadastro();

};

#endif