#include "funcionario.hpp"

bool file_exists_funcionario(const string &name, string CPF){
    CPF += ".txt";
    vector<string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);

    for(string NomeArq : v){
        if(NomeArq == CPF) return true;
    }

    return false;
}

bool text_exists_funcionario(string name, string dado){
    ifstream entrada;
    string texto;

    entrada.open("../ep1/doc/Funcionários/" + name + ".txt");

    if(entrada.is_open()){
        while(getline(entrada, texto)){
            if(texto == dado){
                entrada.close();
                return true;
            }
        }
    }
    return false;
}

Funcionario::Funcionario(){
    set_NomeCompleto("");
    set_CPF("");
    this->senha = "";
}

Funcionario::~Funcionario(){

}

void Funcionario::set_senha(string senha){
    this->senha = senha;
}

string Funcionario::get_senha(){
    return senha;
}

bool Funcionario::cadastro(){
    ofstream saida;
    ifstream entrada;
    string texto;
    string y_or_n, cpf, nome_completo;
    int op, cont = 0;

    system("clear");
    cout << "Bem vindo, funcionário. Já possui cadastro? [y/n]" << endl;
    cin >> y_or_n;

    if(y_or_n == "n"){
        cout << "Insira o CPF que deseja cadastrar:" << endl;
        cin >> cpf;
        set_CPF(cpf);
        if(file_exists_funcionario("../ep1/doc/Funcionários/", get_CPF())){
            cout << "Você já está cadastrado!" << endl;
            cout << "Insira a sua senha:" << endl;
            cin >> senha;
            if(!text_exists_funcionario(get_CPF(), senha)){
                do{
                    cont++;
                    cout << "Senha incorreta!" << endl;
                    cout << "Insira a senha novamente:" << endl;
                    cin >> senha;
                    if(cont == 2 && !text_exists_funcionario(get_CPF(), senha)){
                        cout << "Senha incorreta! Esqueceu a senha? [y/n]" << endl;
                        cin >> y_or_n;
                        if(y_or_n == "y"){
                            cadastro();
                        }else{
                            cont = 0;
                            cout << "Insira a senha novamente:" << endl;
                            cin >> senha;
                        }
                    }
                }while(!text_exists_funcionario(get_CPF(), senha));
            }else{
                return true;
            }
            return true;
        }else{
            saida.open("../ep1/doc/Funcionários/" + get_CPF() + ".txt");
            if(saida.is_open()){
                cout << "Insira a senha que deseja cadastrar:" << endl;
                cin >> senha;
                saida << senha;
                saida.close();
                return true;
            }
        }
    }else if(y_or_n == "y"){
        cout << "Insira o CPF cadastrado:" << endl;
        cin >> cpf;
        set_CPF(cpf);
        if(file_exists_funcionario("../ep1/doc/Funcionários/", get_CPF())){
            cout << "Insira a sua senha:" << endl;
            cin >> senha;
            if(!text_exists_funcionario(get_CPF(), senha)){
                do{
                    cont++;
                    cout << "Senha incorreta!" << endl;
                    cout << "Insira a senha novamente:" << endl;
                    cin >> senha;
                    if(cont == 2 && !text_exists_funcionario(get_CPF(), senha)){
                        cout << "Senha incorreta! Esqueceu a senha? [y/n]" << endl;
                        cin >> y_or_n;
                        if(y_or_n == "y"){
                            cadastro();
                        }else{
                            cont = 0;
                            cout << "Insira a senha novamente:" << endl;
                            cin >> senha;
                        }
                    }
                }while(!text_exists_funcionario(get_CPF(), senha));
            }else{
                return true;
            }
            return true;
        }else{
            do{
                cout << "Funcionário não cadastrado!" << endl << "Selecione uma opção:" << endl;
                cout << "1 - Tentar novamente." << endl << "2 - Cadastrar." << endl;
                cin >> op;
                if(op == 1){
                    cout << "Insira o CPF cadastrado:" << endl;
                    cin >> cpf;
                    set_CPF(cpf);
                }else if(op == 2){
                    cout << "Insira o CPF que deseja cadastrar:" << endl;
                    cin >> cpf;
                    set_CPF(cpf);
                    if(file_exists_funcionario("../ep1/doc/Funcionários/", get_CPF())){
                        cout << "Você já está cadastrado!" << endl;
                        cout << "Insira a sua senha:" << endl;
                        cin >> senha;
                        if(!text_exists_funcionario(get_CPF(), senha)){
                            do{
                                cont++;
                                cout << "Senha incorreta!" << endl;
                                cout << "Insira a senha novamente:" << endl;
                                cin >> senha;
                                if(cont == 2 && !text_exists_funcionario(get_CPF(), senha)){
                                    cout << "Senha incorreta! Esqueceu a senha? [y/n]" << endl;
                                    cin >> y_or_n;
                                    if(y_or_n == "y"){
                                        cadastro();
                                    }else{
                                        cont = 0;
                                        cout << "Insira a senha novamente:" << endl;
                                        cin >> senha;
                                    }
                                }
                            }while(!text_exists_funcionario(get_CPF(), senha));
                        }else{
                            return true;
                        }
                        return true;
                    }else{
                        saida.open("../ep1/doc/Funcionários/" + get_CPF() + ".txt");
                        if(saida.is_open()){
                            cout << "Insira a senha que deseja cadastrar:" << endl;
                            cin >> senha;
                            saida << senha;
                            saida.close();
                            return true;
                        }
                    }
                }
            }while(!file_exists_funcionario("../ep1/doc/Funcionários/", get_CPF()));

            cout << "Insira a sua senha:" << endl;
            cin >> senha;
            if(!text_exists_funcionario(get_CPF(), senha)){
                do{
                    cont++;
                    cout << "Senha incorreta!" << endl;
                    cout << "Insira a senha novamente:" << endl;
                    cin >> senha;
                    if(cont == 2 && !text_exists_funcionario(get_CPF(), senha)){
                        cout << "Senha incorreta! Esqueceu a senha? [y/n]" << endl;
                        cin >> y_or_n;
                        if(y_or_n == "y"){
                            cadastro();
                        }else{
                            cont = 0;
                            cout << "Insira a senha novamente:" << endl;
                            cin >> senha;
                        }
                    }
                }while(!text_exists_funcionario(get_CPF(), senha));
            }
            return true;
        }
    }
    return false;
}